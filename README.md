# `go-shlex`

`go-shlex` is a simple lexer for go that supports shell-style quoting, commenting, and escaping.

This was cloned from Google's [`go-shlex`](https://github.com/google/shlex.git).
